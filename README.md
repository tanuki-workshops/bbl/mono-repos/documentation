# Documentation

- https://about.gitlab.com/direction/monorepos/

## "Monorepos features"

- Git partial clone
- Git LFS
- Parent-child pipelines
- Dynamic pipeline creation for artifact includes
- Monorepo package management
